// ==UserScript==
// @name         Contador Ponto
// @namespace    http://tampermonkey.net/
// @version      3.9.1
// @description  Conta a horas a partir da última batida, Calcula a compensação semanal.
// @author       Markin Furkin - Marcos Furquim
// @grant        unsafeWindow
// @grant        GM_addStyle
// @grant        GM_getValue
// @grant        GM_setValue
// @icon         http://dotgroup.com.br/wp-content/themes/dot-group/favicon.png
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js
// @include      https://www.ahgora.com.br/externo/batidas
// @include      https://www.ahgora.com.br/externo/batidas/*
// @downloadURL  https://openuserjs.org/install/markinfurkingmail.com/Contador_Ponto.user.js
// @updateURL    https://openuserjs.org/meta/markinfurkingmail.com/Contador_Ponto.meta.js
// @license      GPL-3.0+; http://www.gnu.org/licenses/gpl-3.0.txt
// ==/UserScript==


(function() {
    'use strict';
    function fixedPaddingRightModal() {
        var fixedCls = '#contadorFlutuante';
        var oldSSB = $.fn.modal.Constructor.prototype.setScrollbar;
        $.fn.modal.Constructor.prototype.setScrollbar = function () {
            oldSSB.apply(this);
            if (this.bodyIsOverflowing && this.scrollbarWidth)
                $(fixedCls).css('right', this.scrollbarWidth+parseInt($(fixedCls).css("right")));
        };

        var oldRSB = $.fn.modal.Constructor.prototype.resetScrollbar;
        $.fn.modal.Constructor.prototype.resetScrollbar = function () {
            oldRSB.apply(this);
            $(fixedCls).css('right', '');
        };
    }
    function getHorasParte(horas) { /* formato "hh:mm */
        return parseInt(horas.substr(0,horas.indexOf(":")));
    }
    function getDataHoje() {
        var hoje = new Date();
        var dd = hoje.getDate();
        var mm = hoje.getMonth()+1; //January is 0!
        var yyyy = hoje.getFullYear();

        if(dd<10) {
            dd='0'+dd;
        }

        if(mm<10) {
            mm='0'+mm;
        }

        //hoje = dd+'/'+mm+'/'+yyyy;
        hoje = dd+'/'+mm;
        return hoje;
    }
    function getArrayDiasDaSemana() {
        var hoje = new Date();
        //console.log(hoje);
        var diaHoje = hoje.getDay();
        var diferenca = hoje.getDate() - diaHoje + (diaHoje === 0 ? -6:1); // adjuste se for domingo
        var segunda = new Date(hoje.setDate(diferenca));
        //console.log("segunda"+segunda);
        //var dd = segunda.getDate();
        //var mm = segunda.getMonth()+1; //January is 0!
        //var yyyy = segunda.getFullYear();
        var ArraySemana = [];
        var i = 0;
        var dataCorrente = new Date();

        for(i=0;i<5;i++) {
            dataCorrente.setTime(segunda.getTime()+(i*24*60*60*1000));
            //console.log("dataCorrente"+dataCorrente);
            var dd = dataCorrente.getDate();
            var mm = dataCorrente.getMonth()+1; //January is 0!
            var yyyy = dataCorrente.getFullYear();
            var ddText = (dd<10)? '0'+dd: dd;
            var mmText = (mm<10)? '0'+mm: mm;
            //ArraySemana.push(ddText+'/'+mmText+'/'+yyyy);
            ArraySemana.push(ddText+'/'+mmText);
        }
        return ArraySemana;
    }
    function getArrayCompensacaoSemanal(tempoReal) {
        var ArrayDiasDaSemana = getArrayDiasDaSemana();
        tempoReal = (typeof tempoReal === 'undefined') ? false : tempoReal;
        //if(!$('#ckbCompensacaoReal').is(":checked")) {
        if(!tempoReal) {
            ArrayDiasDaSemana.splice(ArrayDiasDaSemana.indexOf(getDataHoje()),1); //remove o ultimo dia da contagem, pois nao esta ativado o tempo real
        }
        var arrayCompensacaoSemanal = [];
        for (var i=0;i<ArrayDiasDaSemana.length;i++) {
            /* Metodo antigo - capturando via texto da td */
            /*var tr =  $("#tableTotalize").next().next().find("tr>td:first-child:contains("+ArrayDiasDaSemana[i]+")");
            var td = tr.siblings().eq(5).text().trim();
            var horasCompensacaoRegex = /(?:SEMANAL: )(-*[0-9]+:[0-9]+)/ig;
            var arrayRegexMatch = horasCompensacaoRegex.exec(td);
            if(arrayRegexMatch !== null) {
                arrayRegexMatch.splice(0,1); //remover posicao 0, que armazena a expressao regex
                arrayCompensacaoSemanal.push(arrayRegexMatch[0]);
            } */
            //console.log(ArrayDiasDaSemana[i]);
            var totalTrabalhadoDia = getTotalHorasTrabalhadas(ArrayDiasDaSemana[i]);
            //console.log("totalTrabalhadoDia",totalTrabalhadoDia);
            //compensacaoDia = calculaHoras("08:00",getTotalHorasTrabalhadas(ArrayDiasDaSemana[i]),true);
            var compensacaoDiaDiff = moment(totalTrabalhadoDia,"HH:mm").diff(moment("0"+$("#inputConfigHorasDia").val()+":00","HH:mm"));
            //console.log("compensacaoDiaDiff",compensacaoDiaDiff);
            var compensacaoDiaDuracao = moment.duration(compensacaoDiaDiff);
            //console.log("compensacaoDiaDuracao",compensacaoDiaDuracao);
            if(compensacaoDiaDuracao._isValid) {
               var compensacaoDiaDuracaoFormatdo = compensacaoDiaDuracao.hours() + ":" + compensacaoDiaDuracao.minutes();
                //console.log("compensacaoDiaDuracaoFormatdo",compensacaoDiaDuracaoFormatdo);
                 //verifica se tem algum abono ou consulta
                /******** CONSULTA **************************************/
                 var consulta = getConsultaSophia(ArrayDiasDaSemana[i]);
                 if(consulta !== undefined) {
                     //console.log("tem consulta para abater");
                     //console.log("consulta:",consulta);
                     var somaComConsulta = calculaHoras(compensacaoDiaDuracaoFormatdo,consulta);
                     //console.log("somaComConsulta",somaComConsulta);
                     compensacaoDiaDuracaoFormatdo = somaComConsulta;
                 }
                /******************************************************/
                 /******** SOPHIA **************************************/
                 var sophia = getConsultaSophia(ArrayDiasDaSemana[i],true);
                 if(sophia !== undefined) {
                     //console.log("tem sophia para abater");
                     //console.log("sophia:",consulta);
                     var somaComSophia = calculaHoras(compensacaoDiaDuracaoFormatdo,sophia);
                     //console.log("somaComSophia",somaComSophia);
                     compensacaoDiaDuracaoFormatdo = somaComSophia;
                 }
                /******************************************************/
                //console.log("compensacaoDiaDiff",compensacaoDiaDiff);
                //console.log("compensacaoDiaDuracao",compensacaoDiaDuracao);
                //console.log("compensacaoDiaDuracaoFormatdo",compensacaoDiaDuracaoFormatdo);
                //console.log(compensacaoDiaDuracaoFormatdo.indexOf("0:-") > -1);
                if(compensacaoDiaDuracaoFormatdo.indexOf("0:-") > -1) {
                    compensacaoDiaDuracaoFormatdo = compensacaoDiaDuracaoFormatdo.replace(/(\d*):-(\d*)/g,"-$1:$2"); //move o negativo dos minutos para as horas
                    //console.log("ajuste negativo min para hr",compensacaoDiaDuracaoFormatdo);
                }
                var compensacaoDia = compensacaoDiaDuracaoFormatdo.replace(/(\d*):-(\d*)/g,"$1:$2"); //remove o "-" dos minutos
                //console.log("compensacaoDia",compensacaoDia);
                arrayCompensacaoSemanal.push(compensacaoDia);
            }
        }
        return arrayCompensacaoSemanal;
    }
    function getArrayHorasDia(data) {
        var dia = (typeof data === 'undefined') ? getDataHoje() : data;
        //console.log("hoje"+hoje);
        var tr = $("#tableTotalize").next().next().find("tr>td:first-child:contains("+dia+")");
        var horasStr = tr.siblings().eq(1).text().trim().split(",");
        //console.log("Horas Array:"+horasStr);
        return horasStr;
    }
    function getHoraAtual () {
        var data = new Date();
        var horasAgora = data.getHours();
        var minutosAgora = data.getMinutes();
        if(minutosAgora<10) {
            minutosAgora='0'+minutosAgora;
        }
        return horasAgora + ":" + minutosAgora;
    }
    function calculaHoras(inicio, fim, subtracao) { /* formato "hh:mm */
        //verifica se numero é negativo
        var negativoInicio = (inicio.indexOf("-")>-1)? true: false ;
        var negativoFim = (fim.indexOf("-")>-1)? true: false ;
        //console.log("negativoInicio"+negativoInicio);
        //console.log("negativoFim"+negativoFim);
        inicio = inicio.replace(/-/g,'');
        fim = fim.replace(/-/g,'');
        var inicioHoras = inicio.substr(0,inicio.indexOf(":"));
        var inicioMinutos = inicio.substr(inicio.indexOf(":")+1);
        //console.log("Horas inicio:"+inicioHoras);
        //console.log("Minutos inicio:"+inicioMinutos);
        var fimHoras = fim.substr(0,fim.indexOf(":"));
        var fimMinutos = fim.substr(fim.indexOf(":")+1);
        //console.log("Horas fim:"+fimHoras);
        //console.log("Minutos fim:"+fimMinutos);
        var totalInicioMinutos = (inicioHoras*60) + parseInt(inicioMinutos);
        var totalFimMinutos = (fimHoras*60) + parseInt(fimMinutos);

        totalInicioMinutos = (negativoInicio)? totalInicioMinutos * -1: totalInicioMinutos;
        totalFimMinutos = (negativoFim)? totalFimMinutos * -1: totalFimMinutos;
        //console.log("totalInicioMinutos:"+totalInicioMinutos);
        //console.log("totalFimMinutos:"+totalFimMinutos);
        var calcMinutos = 0;
        if(subtracao || ((negativoInicio && !negativoFim) || (!negativoInicio && negativoFim))) {
            //console.log("substracao");
            if((negativoInicio && !negativoFim) || (!negativoInicio && negativoFim)) {
                calcMinutos = totalFimMinutos + totalInicioMinutos;
            } else {
                if(totalFimMinutos > totalInicioMinutos) {
                    //console.log("fim>inicio");
                    calcMinutos = totalFimMinutos - totalInicioMinutos;
                }else{
                    //console.log("inicio>fim");
                    calcMinutos = (parseInt(totalInicioMinutos)) - (parseInt(totalFimMinutos));
                }
            }

        } else {
            calcMinutos = totalFimMinutos + totalInicioMinutos;
        }
        //console.log("calcMinutos:"+calcMinutos);
        //var calcMinutos =  (subtracao)?  totalFimMinutos - totalInicioMinutos :  totalFimMinutos + totalInicioMinutos;
        //console.log("diferenca em minutos"+diferencaMinutos);
        var minutos = calcMinutos % 60;
        var horas = Math.floor(Math.abs(calcMinutos) / 60);
        //console.log(minutos);
        //console.log(horas);
        if(minutos<10 && minutos>-1) {
            minutos='0'+minutos;
        }
        return (calcMinutos<0)? "-"+horas + ":" + minutos : horas + ":" + minutos;
    }
    function getTotalHorasTrabalhadas(data) { //formato dd/mm/yyyy
        //console.group("dia:" + data);
        var horas = getArrayHorasDia(data);
        //console.log("horas:"+horas);
        var horasTrabalhadasTotal = 0;
        //console.log("length array:"+horas.length);
        var horasTrabalhadas = [];
        var i=0;
        var j=0;
        for(i=0;i<horas.length;i+=2) {
            horasTrabalhadas[j] = "0:0";
            if(i==horas.length-1) { /* ultima batida */
                horasTrabalhadas[j] = calculaHoras(horas[i],getHoraAtual(),true);
            } else {
                horasTrabalhadas[j] = calculaHoras(horas[i],horas[i+1],true);
            }
            if(horasTrabalhadas.length>1) {
                horasTrabalhadasTotal = calculaHoras(horasTrabalhadas[j],horasTrabalhadasTotal,false);
            } else {
                horasTrabalhadasTotal = calculaHoras(horasTrabalhadas[j],"0:0",false);
            }
            j++;
        }
        horasTrabalhadasTotal = (getHorasParte(horasTrabalhadasTotal)>=10)? "10:00":horasTrabalhadasTotal;
        //console.log("horasTrabalhadasTotal:"+horasTrabalhadasTotal);
        //console.log("horas",getHorasParte(horasTrabalhadasTotal));
        //console.groupEnd();
        return horasTrabalhadasTotal;
    }
    function getCompensacaoSemanal(tempoReal) {
        tempoReal = (typeof tempoReal === 'undefined') ? false : tempoReal;
        //HORAS DA SEMANA - COMPENSAÇÃO SEMANAL
        //console.log("CALCULANDO COMPENSACAO");
        var arrayCompensacaoSemanal = [];

        arrayCompensacaoSemanal = getArrayCompensacaoSemanal(tempoReal);
        //console.group("arrayCompensacaoSemanal");
        //console.log("arrayCompensacaoSemanal"+arrayCompensacaoSemanal);
        //FAZ O CALCULO
        var compensacaoSemanal = "00:00";
        for(var i=0;i<arrayCompensacaoSemanal.length;i++) {
            if(compensacaoSemanal>=0 && arrayCompensacaoSemanal[i].indexOf("-")>-1) {
                //arrayCompensacaoSemanal.replace(/-/g,'');
                //console.log("Negativo");
                compensacaoSemanal = calculaHoras(compensacaoSemanal,arrayCompensacaoSemanal[i],true);
                //console.log(totalCompensacaoSemanal);
            } else {
                compensacaoSemanal = calculaHoras(compensacaoSemanal,arrayCompensacaoSemanal[i]);
            }
        }
        //console.log("totalCompensacaoSemanal",totalCompensacaoSemanal);
        //console.groupEnd();
        return compensacaoSemanal.replace(/(\d*):-(\d*)/g,"$1:$2"); //remove o "-" dos minutos
    }
    function montaDivFlutuante() {
        var svgLogin = "<svg xmlns='http://www.w3.org/2000/svg' width='25px' height='25px' viewBox='0 0 48 48' aria-hidden='true'><path d='M24,0C10.74,0 0,10.74 0,24C0,37.26 10.74,48 24,48C37.26,48 48,37.26 48,24C48,10.74 37.26,0 24,0ZM24,41.28C17.988,41.28 12.708,38.208 9.6,33.552C9.66,28.788 19.212,26.16 24,26.16C28.788,26.16 38.328,28.788 38.4,33.552C35.292,38.208 30.012,41.28 24,41.28ZM24,7.2C27.972,7.2 31.2,10.428 31.2,14.4C31.2,18.384 27.972,21.6 24,21.6C20.028,21.6 16.8,18.384 16.8,14.4C16.8,10.428 20.028,7.2 24,7.2Z'></path><path d='M0 0h48v48H0z' fill='none'></path></svg>";
        var div = $("<div></div>").attr("id","contadorFlutuante");
        div.append("<div id='configPonto'>" +
                   "<input type='text' id='matricula' placeholder='Matrícula'/>" +
                   "<input id='senha' placeholder='Senha' type='password'/>" +
                   "<button class='batePonto btn btn-primary'>Bater Ponto</button>" +
                   "</div>" +
                   "<div id='head'>" +
                   "<button class='seta esconde'></button>" +
                   "<div class='svg-wrapper'><div class='svg'>"+svgLogin+"</div></div>" +
                   "<div id='divCog'>" +
                   "<button type='button' class='btn btn-default' data-toggle='modal' data-target='#modalConfigS'>" +
                   "<span class='glyphicon glyphicon-cog'></span>" +
                   "</button>" +
                   "</div>" +
                   "</div>");
        div.append("<div id='dados'>-</div>");
        div.appendTo("body");/*.draggable(
            {
                stop: function(event, ui)
                {
                    var top = getTop(ui.helper);
                    ui.helper.css('position', 'fixed');
                    ui.helper.css('top', top+"px");
                }
            }).resizable(
            {
                stop: function(event, ui)
                {
                    var top = getTop(ui.helper);
                    ui.helper.css('position', 'fixed');
                    ui.helper.css('top', top+"px");
                }
            });*/
        cssInjection();
    }
    function getTop(ele) {
        var eTop = ele.offset().top;
        var wTop = $(window).scrollTop();
        var top = eTop - wTop;

        return top;
    }
    function cssInjection() {
        $("<style></style").html("#contadorFlutuante {position: fixed;z-index: 9999;bottom:1%;right:20px;background-color: rgba(51, 122, 183, 0.8);color: #fff;border: 5px solid rgb(60, 104, 140);border-radius: 6px;background-clip: padding-box; width:310px; transition: bottom 0.5s; -webkit-transition: bottom 0.5s;}" +
                                 "#contadorFlutuante #configPonto {display:none; background-color: #3c688d;padding-bottom: 5px;}" +
                                 "#contadorFlutuante #configPonto.escondido { -webkit-animation: all 0.5s; animation: all 0.5s; transition: all 0.5s; -webkit-transition: all 0.5s;}" +
                                 "#contadorFlutuante #configPonto input {background-color: #ffffff; display: inline-block; width: 50%; border: 4px solid #3c688c; color: #000; border-radius:4px;}" +
                                 "#contadorFlutuante #configPonto input::-webkit-input-placeholder { color:#c1c1c1;}" +
                                 "#contadorFlutuante #configPonto input::-moz-placeholder { color:#c1c1c1;}" +
                                 "#contadorFlutuante #configPonto input:-webkit-autofill { -webkit-box-shadow: 0 0 0 30px #5a93c4 inset; -webkit-text-fill-color: #fff !important;}" +
                                 "#contadorFlutuante #ckbAlerta {background-color: #5b93c0;appearance: none;-moz-appearance: none;-webkit-appearance: none;width: 20px;height: 20px;border: 2px #c3afaf solid;margin: 0px; vertical-align: middle; cursor:pointer;}" +
                                 "#contadorFlutuante #ckbAlerta + label {margin-left:5px}" +
                                 "#contadorFlutuante #ckbAlerta:checked:after{content:'\\2714'; font-size:20px; position:absolute; color:#214f71; top: -4px;}" +
                                 "#modalConfigS input[type='checkbox'] {background-color: #5b93c0;appearance: none;-moz-appearance: none;-webkit-appearance: none;width: 20px;height: 20px;border: 2px #c3afaf solid;margin: 0px; vertical-align: middle; cursor:pointer; text-apparence:none;outline:none;}" +
                                 "#modalConfigS input[type='checkbox'] + label {margin-left:5px}" +
                                 "#modalConfigS input:checked:after{content:'\\2714'; font-size:20px; position:absolute; color:#214f71; top: -6px;}" +
                                 "#modalConfigS .toDo { margin-top: 30px;padding-top: 30px;border-top: 2px solid #376388;}"+
                                 "#modalConfigS .toDo p { font-weight: 500;}" +
                                 "#modalConfigS .version { position: absolute;bottom: 0;padding-left: 5px;font-size: 10px; } " +
                                 "#modalConfigS .version span { font-weight: 700;font-size: 12px; } " +
                                 "#contadorFlutuante #head {background-color: rgba(53, 93, 127, 0.8);}" +
                                 "#contadorFlutuante #head:after {clear: both; content: ''; display: block;}" +
                                 "#contadorFlutuante #dados {padding: 20px 20px 20px 0px; min-height:140px;}" +
                                 "#contadorFlutuante.escondido {bottom:-145px;}" +
                                 "#contadorFlutuante p { text-align:right}" +
                                 "#contadorFlutuante p span{color:#fff;font-size:2em;font-weight:bold;}" +
                                 "#contadorFlutuante button.batePonto { width: 100%;}" +
                                 "#contadorFlutuante button.batePonto.disabled {cursor:not-allowed;}" +
                                 "#contadorFlutuante button.seta {background-color: transparent; border: none; box-sizing: border-box; display: inline-block;float: left;}" +
                                 "#contadorFlutuante button.seta:focus,#contadorFlutuante input:focus {outline: none;}" +
                                 "#contadorFlutuante button.seta.esconde { }" +
                                 "#contadorFlutuante button.seta.esconde:after { content: '\\25BC'; font-size: 20px;}" +
                                 "#contadorFlutuante button.seta.esconde:hover:after { color:#5b93c0;}" +
                                 "#contadorFlutuante button.seta.mostra { }" +
                                 "#contadorFlutuante button.seta.mostra:after { content: '\\25B2'; font-size: 20px;}" +
                                 "#contadorFlutuante button.seta.mostra:hover:after { color:#5b93c0;}" +
                                 "#contadorFlutuante .svg-wrapper {display:inline-block; vertical-align: top; width:80%; text-align: center;}" +
                                 "#contadorFlutuante .svg {display:inline-block; vertical-align: bottom text-align: center; cursor:pointer; fill:#fff;}" +
                                 "#contadorFlutuante .svg:hover {fill:#5b93c0;}" +
                                 "#contadorFlutuante #divCog {position: relative; display: inline-block; text-align: right; width: auto; vertical-align: top;}" +
                                 "#contadorFlutuante #divCog button {background-color: transparent; border: none; color: #fff; padding:0; font-size:21px; outline:0}" +
                                 "#contadorFlutuante #divCog button span.glyphicon:hover {color:#5b93c0;}" +
                                 "#tooltip-contador {position:absolute; position:fixed; overflow:hidden; padding: 15px; color: #fff; background-color: #3c688d; border-radius: 10px; border: 2px solid #fff; z-index: 10000;}" +
                                 "body.modal-open #contadorFlutuante {/*right:37px;*/}" +
                                 ".minTotal {font-weight:700;display:block;}" +
                                 ".minTotal n {font-size: 1.2em;}" +
                                 ".configHorasDia input[type=number] {color: #335f84;width: 40px;}" +
                                 "body.darkTheme #content {background-color:#000; color: #fff;}" +
                                 "body.darkTheme .label-success { background-color: #005800;}" +
                                 "body.darkTheme .label-info { background-color: #0f404e;}" +
                                 "body.darkTheme .label-warning { background-color: #8a5100;}" +
                                 "body.darkTheme .table-striped>tbody>tr:nth-of-type(odd) {background-color: #403e3e;}" +
                                 "body.darkTheme .table>thead>tr>th { background-color: #001325;}" +
                                 "body.darkTheme .table>tbody>tr>td.success {background-color: #15250e; color: #abe4ad}" +
                                 "body.darkTheme .table>tbody>tr.warning>td { background-color: #8a5100; color: #e0be83;}" +
                                 "body.darkTheme .modal-content {background-color: #000; border-color: #fff;}" +
                                 "body.darkTheme #modalConfigS {color:#fff;}" +
                                 "body.darkTheme #attachModal {color:#fff;}" +
                                 "body.darkTheme #type_abono {color:#000;}" +
                                 "body.darkTheme .well  {color:#555;}" +
                                 "body.darkTheme #accordion {background-color:#000}" +
                                 "body.darkTheme .list-group-item {background-color:#000!important}" +
                                 "#tooltip-contador b {font-size:1.3em;}").appendTo("head");


    }
    function notificacao(titulo, mensagem, iconLink) {
        if(typeof notification == 'undefined') {
            window.notification = null;
        }
        if(notification !== null) {
            notification.close();
        }

        var options = {
            body: mensagem,
            icon: iconLink
        };
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
            alert("This browser does not support desktop notification");
        }

        // Let's check whether notification permissions have already been granted
        else if (Notification.permission === "granted") {
            // If it's okay let's create a notification
            notification = new Notification(titulo,options);
        }

        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                // If the user accepts, let's create a notification
                if (permission === "granted") {
                    notification = new Notification(titulo,options);
                }
            });

        }
    }
    var doisPontosBlinkInterval;
    function doisPontosBlink() {
        $("#contadorFlutuante t.doisPontos").css('visibility', 'hidden');
        doisPontosBlinkInterval = setInterval(
            function() {
                if($("#contadorFlutuante t.doisPontos").css('visibility')=='hidden')
                {
                    $("#contadorFlutuante t.doisPontos").css('visibility','visibility');
                }else {
                    $("#contadorFlutuante t.doisPontos").css('visibility','hidden');
                }
            },600
        );

    }
    function eventsTrigger() {
        $("#contadorFlutuante button.seta").click(function(){
            if($(this).hasClass('esconde')) {
                $(this).removeClass('esconde').addClass('mostra');
                $("#contadorFlutuante").toggleClass('escondido');
                //$("#contadorFlutuante").stop().animate({bottom:'-115px'},{queue:false,duration:500});
            }
            else if ($(this).hasClass('mostra')) {
                $(this).removeClass('mostra').addClass('esconde');
                $("#contadorFlutuante").toggleClass('escondido');
                // $("#contadorFlutuante").stop().animate({bottom:'1%'},{queue:false,duration:500});
            }
        });

        $("#contadorFlutuante button.batePonto").click(function(){
            if(validaCamposBatida()) {
                if(confirm("Bater o Ponto?")) {
                    var account = $("#contadorFlutuante #matricula").val();
                    var password = $("#contadorFlutuante #senha").val();
                    ajaxBatePonto(account,password);
                }
            }
        });

        $("#contadorFlutuante #configPonto input").keyup(function() {
            BotaoBaterValida();
        });
        $("#contadorFlutuante #configPonto input").change(function() {
            BotaoBaterValida();
        });

        $("#contadorFlutuante .svg").click(function(){
            $("#contadorFlutuante #configPonto").toggle();
            BotaoBaterValida();
        });

        $("#modalConfigS-btn-salvar").click(function() {
            $("#modalConfigS input[type='checkbox']").each(function () {
                salvaDadosCookie($(this).attr("id"),$(this).is(':checked'));
            });
            start();
            $("#modalConfigS").modal('hide');
        });

        $("#ckbDarkTheme").click(function() {
            if($(this).is(':checked')) {
                $("body").addClass("darkTheme");
            } else {
                $("body").removeClass("darkTheme");
            }
        });
        $("#ckbMinTotal").click(function() {
            if($(this).is(':checked')) {
              minTotal();
            } else {
               $(".minTotal").remove();
            }
        });

    }
    function validaCamposBatida() {
        if (($("#contadorFlutuante #matricula").val().length !== 0) && ($("#contadorFlutuante #senha").val().length !== 0)) {
            //console.log("campos preenchidos");
            return true;
        } else {
            return false;
        }
    }
    function BotaoBaterValida() {
        //console.log("validando botao");
        if(validaCamposBatida()) {
            $("#contadorFlutuante button.batePonto").removeClass("disabled");
        } else {
            $("#contadorFlutuante button.batePonto").addClass("disabled");
        }
    }
    function ajaxBatePonto(account,password) {
        var dados = "account="+account+"&password="+password+"&identity=a9b0832cc1c3d897b1081f16237ede9a&key=";
        $.ajax({
            type: "POST",
            url: "https://www.ahgora.com.br/batidaonline/verifyIdentification",
            data: dados,
            success: function(data) {
                if(!data.result && data.reason == "inativo") {
                    ahg.msg_err(_('Usuário inativo, contate o departamento de RH.'));
                }else if(!data.result && data.reason == "not_in_ip_range") {
                    ahg.msg_err(_('Você não está na faixa de IP determinada.'));
                }else if(!data.result && data.reason == "employee_is_fired") {
                    ahg.msg_err(_('Usuário não possui permissão para batida, contate o RH por favor'));
                }else if(!data.result && data.reason == "not_found") {
                    ahg.msg_err(_('Dados incorretos.'));
                } else if(!data.result && (data.reason == "employee_dont_list" || data.reason == "location_dont_list")) {
                    ahg.msg_err(_('Este funcionário não possui permissão para utilizar este dispositivo.'));
                } else if(!data.result) {
                      ahg.msg_err(_('Erro'));
                }else if(data.result) {
                    if(data.batidas_dia) {
                        var batidas = '';
                        $.each(data.batidas_dia, function(n, v){
                            batidas += v.substr(0, 2)+':'+v.substr(2, 2)+' ';
                        });
                        ahg.msg_suc("<span class='glyphicon glyphicon-calendar'></span> Batidas do dia para "+data.nome+": "+batidas, 30000);
                    }
                    ahg.msg_suc("<span class='glyphicon glyphicon-ok'></span> Batida registrada com sucesso às" + " " + $.formataHora(data.time), 20000);
                    adicionaBatida($.formataHora(data.time));
                }
            }
        });
    }
    function adicionaBatida(horaBatida) {
        var dia =  getDataHoje();
        var tr = $("#tableTotalize").next().next().find("tr>td:first-child:contains("+dia+")");
        var horasStr = tr.siblings().eq(1).text().trim();
        validaBatidas();
        //animando coloração
        var oldBgc =  tr.siblings().eq(1).css("background-color");
        //console.log(oldBgc);
        tr.siblings().eq(1).text(horasStr+", "+horaBatida).animate({backgroundColor: "#5b94c4"}, 'slow', function () {
            tr.siblings().eq(1).animate({backgroundColor: oldBgc}, 'slow');
        });
        tr.siblings().eq(1).text(tr.siblings().eq(1).text().replace(/^,(.*)/,"$1")); //limpa ',' caso seja a primeira batida
        start();

    }
    function salvaDadosCookie(key, value) {
        document.cookie=key+"="+value+";path=/;";
    }
    function carregaCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }
    function deleteCookie(key) {
        setCookie({name: key, value: "", seconds: 1});
    }
    function montaModalConfig() {
        var modalConfig =    $("<div class='modal fade' id='modalConfigS' tabindex='-1' role='dialog' aria-hidden='true'>" +
                               "            <div class='modal-dialog'>" +
                               "                <div class='modal-content'>" +
                               "                    <div class='modal-header'>" +
                               "                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>" +
                               "                        <h4 class='modal-title'><span class='glyphicon glyphicon-cog'></span> Configurações Contador Ponto</h4>" +
                               "                    </div>" +
                               "                    <form role='form' class='form-horizontal'>" +
                               "                        <div class='modal-body'>" +
                               "							<div class='configHorasDia'>Horas trabalhadas por dia: <input id='inputConfigHorasDia' min=4 max=10 type='number' value=8></div>" +
                               "                            <div class='checkbox'>" +
                               "                               <input type='checkbox' id='ckbAlerta8'><label for='ckbAlerta8'>Alerta de 8 horas trabalhadas</label>" +
                               "                            </div>" +
                               "                            <div class='checkbox'>" +
                               "                               <input type='checkbox' id='ckbAlerta1Inter'><label for='ckbAlerta1Inter'>Alerta de 1 hora de intervalo</label>" +
                               "                            </div>" +
                               "                            <div class='checkbox'>" +
                               "                               <input type='checkbox' id='ckbCompensacaoReal'><label for='ckbCompensacaoReal'>Compensação semanal em tempo real (contando do dia corrente)</label>" +
                               "                            </div>" +
                               "                            <div class='checkbox'>" +
                               "                               <input type='checkbox' id='ckbMinTotal'><label for='ckbMinTotal'>Calcular horas trabalhadas em minutos</label>" +
                               "                            </div>" +
                               "                            <div class='checkbox'>" +
                               "                               <input type='checkbox' id='ckbDarkTheme'><label for='ckbDarkTheme'>Dark Theme</label>" +
                               "                            </div>" +
                               "                        </div>" +
                               "                        <div class='modal-footer'>" +
                               "                            <button type='button' class='btn btn-danger' data-dismiss='modal'><span class='glyphicon glyphicon-remove'></span>Cancelar</button>" +
                               "                            <button type='button' id='modalConfigS-btn-salvar' class='btn btn-primary'><span class='glyphicon glyphicon-check'></span>Salvar</button>" +
                               "                        </div>" +
                               "                        <div class='version'>" +
                               "                               Versão <span>3.9</span>"+
                               "                        </div>" +
                               "                    </form>" +
                               "                </div>" +
                               "            </div>" +
                               "        </div>");
        modalConfig.appendTo("body");
        carregaConfigsModal();

    }
    function carregaConfigsModal() {
        $("#modalConfigS input[type='checkbox']").each(function () {
            var value = (carregaCookie($(this).attr("id"))=='true');
            //console.log($(this),value);
            $(this).prop('checked', value);
        });
        darkTheme();
        verificaMinTotal();

    }
    function validaBatidas() {
        clearInterval(doisPontosBlinkInterval);
        var horas = getArrayHorasDia();
        //console.log("horsArray",horas);
        if(horas.length % 2 > 0  && horas[0].length > 0) { //impar
            //Tempo correndo, pisca dois pontos dos relógios
            doisPontosBlink();
            //VERIFICA SE JÁ DEU 8 HORAS, SE SIM, MANDA UMA NOTIFICACAO
            //console.log("mais de 8 horas diario",parseInt(horasTrabalhadasTotal.substr(0,horasTrabalhadasTotal.indexOf(":")))>=8);
            if((parseInt(horasTrabalhadasTotal.substr(0,horasTrabalhadasTotal.indexOf(":")))>=8)) {
                if($('#ckbAlerta8').is(":checked")) {
                    notificacao("Horas trabalhadas",horasTrabalhadasTotal,"http://icon-icons.com/icons2/933/PNG/512/clock-with-white-face_icon-icons.com_72804.png");
                }

            }
        } else { //par
            //verfica se a ultima batida esta em horario de trabalho valido (entre 8 as 17)
            //console.log("ultima batida",horas[horas.length-1].substr(0,horas[horas.length-1].indexOf(":")));
            if(parseInt(horas[horas.length-1].substr(0,horas[horas.length-1].indexOf(":")))> 8  &&
               parseInt(horas[horas.length-1].substr(0,horas[horas.length-1].indexOf(":")))< 17) {
                //verifica se de mais de 1 hora de intervalo
                var horaAgora = getHoraAtual();
                var diferencaIntervalo = moment(getHoraAtual(),"HH:mm").diff(moment(horas[horas.length-1],"HH:mm"));
                var diferencaIntervaloDuracao = moment.duration(diferencaIntervalo);
                //console.log("diferencaIntervaloDuracao",diferencaIntervaloDuracao);
                var diferencaIntervaloDuracaoMin = diferencaIntervaloDuracao.asMinutes();
                if($('#ckbAlerta1Inter').is(":checked") && (diferencaIntervaloDuracaoMin >= 60)) {
                    var horastxt = (diferencaIntervaloDuracao.hours() <10)? "0"+diferencaIntervaloDuracao.hours():diferencaIntervaloDuracao.hours();
                    var minutostxt = (diferencaIntervaloDuracao.minutes() <10)? "0"+diferencaIntervaloDuracao.minutes():diferencaIntervaloDuracao.minutes();
                    var diferencaIntervaloDuracaoFormatado = horastxt + ":" + minutostxt;
                    notificacao("Intervalo",diferencaIntervaloDuracaoFormatado,"http://icon-icons.com/icons2/37/PNG/96/alerts_bell_4128.png");
                }
            }
            //clearInterval(doisPontosBlinkInterval);
        }
    }
    var tooltip = {
        montar: function () {
            var tooltip = $("<div id='tooltip-contador'>00:00</div>");
            tooltip.appendTo("body");
            getHoraIdealSaida();
        },
        posicionar: function () {
            $('#contadorFlutuante #dados').hover(
                function () {
                    $('#contadorFlutuante #dados').mousemove(function(e) {
                        var x = e.clientX,
                            y = e.clientY,
                            offsetX = -70,
                            offsetY = 10,
                            tooltip = $('#tooltip-contador');
                        tooltip.css ({
                            top: (y + offsetY) + 'px',
                            left: (x + offsetX) + 'px',
                            display: "block"
                        });
                    }).css("cursor","help");
                },
                function(e) {
                    $('#tooltip-contador').unbind('mousemove').css("display","none");
                });
            $('#contadorFlutuante #dados').mouseleave(
                function() {
                    //$('#contadorFlutuante #dados').css("display","none");
                });
        }
    };
    function getHoraIdealSaida() {
        var totalTrabalhadoDia = getTotalHorasTrabalhadas();
        var DiaDiff =moment(totalTrabalhadoDia,"HH:mm").diff(moment("0"+$("#inputConfigHorasDia").val()+":00","HH:mm"));
        var DiaDuracao = moment.duration(DiaDiff);
        //console.log("DiaDuracao",DiaDuracao);
        if(DiaDuracao._isValid) {
            //console.log("horas",Math.abs(DiaDuracao.hours()));
            //console.log("minutos",Math.abs(DiaDuracao.minutes()));
            var minTotal = (Math.abs(DiaDuracao.hours())*60) +  Math.abs(DiaDuracao.minutes());
            //console.log("minTotal",minTotal);
            var horaIdealSaida = null;
            if(DiaDuracao._milliseconds < 0) {
                horaIdealSaida = moment().add(minTotal,"m").format("HH:mm");
            } else {
                var horas = getArrayHorasDia();
                //console.log("horsArray",horas);
                if(horas.length % 2 > 0  && horas[0].length > 0) { //impar
                    //console.log("já deveria ter saido");
                    horaIdealSaida = moment().subtract(minTotal,"m").format("HH:mm");
                } else {
                    //console.log("já saiu");
                    //console.log("ultima batida do dia",horas[horas.length-1]);
                    horaIdealSaida = moment(horas[horas.length-1],"HH:mm").subtract(minTotal,"m").format("HH:mm");
                }
            }
            //console.log("horaIdealSaida",horaIdealSaida);
            //console.log("totalCompensacaoSemanal",totalCompensacaoSemanal);
            var totalCompensacaoSemanalLimpo = getCompensacaoSemanal();
            //console.log("totalCompensacaoSemanalLimpo",totalCompensacaoSemanalLimpo);
            //var minutostxt = (horaIdealSaidaCS.minutes() <10)? "0"+horaIdealSaidaCS.minutes():horaIdealSaidaCS.minutes();
            //var totalCompensacaoSemanalLimpoFormatado = horastxt + ":" + minutostxt;
            var horaIdealSaidaCS;
            if(totalCompensacaoSemanalLimpo.indexOf("-")>-1) {
                //console.log("devendo, soma");
                totalCompensacaoSemanalLimpo = totalCompensacaoSemanalLimpo.replace(/-/g,'');
                horaIdealSaidaCS = moment.duration(horaIdealSaida,"HH:mm").add(totalCompensacaoSemanalLimpo,"HH:mm");
            } else {
                //console.log("sobrando, subtrai");
                horaIdealSaidaCS = moment.duration(horaIdealSaida,"HH:mm").subtract(totalCompensacaoSemanalLimpo,"HH:mm");
            }
            //console.log("horaIdealSaidaCS",horaIdealSaidaCS);
            var horastxt = (horaIdealSaidaCS.hours() <10)? "0"+horaIdealSaidaCS.hours():horaIdealSaidaCS.hours();
            var minutostxt = (horaIdealSaidaCS.minutes() <10)? "0"+horaIdealSaidaCS.minutes():horaIdealSaidaCS.minutes();
            var horaIdealSaidaCSFormatado = horastxt + ":" + minutostxt;
            //horaIdealSaidaCSFormatado = horaIdealSaidaCSFormatado.replace(/(.*)<.*:.*>(.*)/,"$1:$2");
            $('#tooltip-contador').html("Saída Ideal: <b>" + horaIdealSaida+ "</b><br/>Considerando a compensação semanal: <b>"+horaIdealSaidaCSFormatado+"</b>");
        }
    }
    function darkTheme() {
        if($("#ckbDarkTheme").is(':checked')) {
            $("body").addClass("darkTheme");
        } else {
            $("body").removeClass("darkTheme");
        }
        //salvaDadosCookie($("#ckbDarkTheme").attr("id"),$("#ckbDarkTheme").is(':checked'));
    }
    function verificaMinTotal() {
	  	if($("#ckbMinTotal").is(':checked')) {
          minTotal();
        } else {
           $(".minTotal").remove();
        }
    }
    function minTotal() {
        $("#tableTotalize").next().next().find("tr>td:contains('Horas Trabalhadas')").each(function() {
            var horasTrabalhadasRegex = /(?:Horas Trabalhadas:) *([0-9]+:[0-9]+)/ig;
            //console.log("texto",$(this).text().trim());
            var arrayRegexMatch = horasTrabalhadasRegex.exec(($(this).text().trim()));
            //console.log("regexmatch",arrayRegexMatch);
            if(arrayRegexMatch !== null) {
                arrayRegexMatch.splice(0,1); //remover posicao 0, que armazena a expressao regex
                //console.log(arrayRegexMatch[0]);
                var horas = parseInt(arrayRegexMatch[0].substr(0,arrayRegexMatch[0].indexOf(":")));
                var min = parseInt(arrayRegexMatch[0].substr(arrayRegexMatch[0].indexOf(":")+1));
                //console.log("horas:",horas);
                //console.log("min:",min);
                var totalMin = horas*60+min;
                //console.log("totalMin",totalMin);
                $(this).parent().children("td:first-child").append("<span class='minTotal'><n>"+totalMin+"</n> min. trabalhados</span>");
            }
        });
    }
    function getConsultaSophia(data,horasSophia){
        var horasCompensacaoRegex = (typeof horasSophia === 'undefined')? /(?:CONSULTA: )(-*[0-9]+:[0-9]+)/ig : /(?:HORAS DE SOPHIA: )(-*[0-9]+:[0-9]+)/ig ;
        var tr =  $("#tableTotalize").next().next().find("tr>td:first-child:contains("+data+")");
        var td = tr.siblings().eq(5).text().trim();
        var arrayRegexMatch = horasCompensacaoRegex.exec(td);
        if(arrayRegexMatch !== null) {
            arrayRegexMatch.splice(0,1); //remover posicao 0, que armazena a expressao regex
            //arrayCompensacaoSemanal.push(arrayRegexMatch[0]);
            //console.log(arrayRegexMatch[0]);
            return arrayRegexMatch[0];
        }
    }
    //Start
    var horasTrabalhadasTotal;
    function start() {
        //HORAS DO DIA
        horasTrabalhadasTotal = getTotalHorasTrabalhadas();

        //console.log("Total:"+horasTrabalhadasTotal);
        horasTrabalhadasTotal = (horasTrabalhadasTotal.indexOf("NaN")>-1) ? "0:00": horasTrabalhadasTotal;
        var horasTrabalhadasTotalTexto = horasTrabalhadasTotal.replace(/:/g,"<t class='doisPontos'>:</t>");

        $("#contadorFlutuante #dados").html("<p>HORAS TRABALHADAS HOJE: <span>"+horasTrabalhadasTotalTexto+"</span></p>");

        var compensacaoTempoReal = false;
        if($('#ckbCompensacaoReal').is(":checked")) {
            compensacaoTempoReal = true;
        }
        var totalCompensacaoSemanal = getCompensacaoSemanal(compensacaoTempoReal);
        //console.log(totalCompensacaoSemanal);
        totalCompensacaoSemanal = totalCompensacaoSemanal.replace(/:/g,"<t class='doisPontos'>:</t>");
        //var totalCompensacaoSemanalSplited = totalCompensacaoSemanal.split(":");
        //console.log("teste regex",totalCompensacaoSemanal.match(/:\D*(\d{1})/));
        if(totalCompensacaoSemanal.match(/:.*(\d{1})$/)) {
            totalCompensacaoSemanal = totalCompensacaoSemanal.replace(/(:\D*)(\d{1})$/,"$10$2");
        }
        //console.log("totalCompensacaoSemanal",totalCompensacaoSemanal);
        totalCompensacaoSemanal = totalCompensacaoSemanal.replace(/00(.*:.*)00/,"0$100");
        if(!$('#ckbCompensacaoReal').is(":checked")) {
            totalCompensacaoSemanal = totalCompensacaoSemanal.replace(/(.*)<.*:.*>(.*)/,"$1:$2");
        }
        $("#contadorFlutuante #dados").append("<p>COMPENSAÇÃO SEMANAL:  <span>" +totalCompensacaoSemanal+"</span></p>");
        if(totalCompensacaoSemanal.indexOf("-")>-1) { //pinta de vermelho caso a compensacao seja negativa
            $("#contadorFlutuante #dados p:nth-child(2) span").css("color","#dc3b3b");
        }
        validaBatidas();
        getHoraIdealSaida();

    }
    montaDivFlutuante();
    montaModalConfig();
    eventsTrigger();
    start();
    setTimeout(function(){BotaoBaterValida();},1000);
    setInterval(start,30000);
    tooltip.montar();
    tooltip.posicionar();
    fixedPaddingRightModal();

})();